MASM_PATH=/mnt/rk3399/progs/masm

AS=$(MASM_PATH)/bin/asl
P2BIN=$(MASM_PATH)/bin/p2bin
MHMT=$(MASM_PATH)/bin/mhmt
CSUM32=$(MASM_PATH)/bin/csum32

AS_FLAGS= -i include -i src -U -L

TARGET=cmos
TARGET_ROM=obj/$(TARGET).rom

TARGET_SCL=$(TARGET).scl
TARGET_HBT=$(TARGET).$$C

TARGET_SCL_ROM=obj/$(TARGET)_make_scl.rom

TARGET_HBT_P=src/$(TARGET)_make_hobeta.p
TARGET_HBT_SRC=$(TARGET_HBT_P:.p=.a80)

CSUM_BIN=obj/csum32.bin
PACK_ROM=obj/main_pent22_pack.rom

all: $(TARGET_SCL) $(TARGET_HBT)

$(TARGET_SCL): lst obj $(TARGET_SCL_ROM) $(CSUM_BIN)
	cat $(TARGET_SCL_ROM) $(CSUM_BIN) >$@

$(TARGET_HBT): $(TARGET_HBT_P)
	$(P2BIN) $< '$@' -r \$$-\$$ -k

$(TARGET_HBT_P): lst obj obj/target.rom $(TARGET_HBT_SRC)
	$(AS) $(AS_FLAGS) $(TARGET_HBT_SRC) -olist lst/$(subst .p,.lst,$(notdir $@))

obj/target.rom: $(TARGET_ROM)
	cp -v $< $@

$(TARGET_SCL_ROM): lst obj $(PACK_ROM)

$(PACK_ROM): $(TARGET_ROM)
	$(MHMT) -mlz $< $@

$(CSUM_BIN): $(TARGET_SCL_ROM)
	$(CSUM32) $<
	mv `basename $(CSUM_BIN)` $(dir $@)

lst:
	mkdir $@
obj:
	mkdir $@

src/%.p : src/%.a80
	$(AS) $(AS_FLAGS) $< -olist lst/$(subst .p,.lst,$(notdir $@))

obj/%.rom : src/%.p
	$(P2BIN) $< $@ -r \$$-\$$ -k

%.$$C : src/%.p
	$(P2BIN) $< $@ -r \$$-\$$ -k


clean:
	rm -rfv lst obj '$(TARGET_SCL)' '$(TARGET_HBT)'
